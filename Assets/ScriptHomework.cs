using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ScriptHomework : MonoBehaviour
{
    [Header("Semaforo imagen cambio de colores")]
    public Image semaforo;
    [Header("Lista de colores del semaforo")]
    public List<Color> coloresSemaforo;
    [Header("Tiempo 1-10")]
    [Range(1.0f,10.0f)] public float timecolor= 1.0f;
    [Header("Semaforo en funcionamiento")]
    public bool semaforoActivo = false;

    public float time=0;
    public int index = 0;

    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(ActivarSemaforo());
        //Pruebita();

    }

    // Update is called once per frame
    void Update()
    {
        if (semaforoActivo)
        {
            if (time>=timecolor)
            {
                time = 0;
                semaforo.color = coloresSemaforo[index];
                index++;
                if (index==coloresSemaforo.Count)
                {
                    semaforoActivo = false;
                    index = 0;
                }     
            }
            else
            {
                time += Time.deltaTime;
            }
           
        }
    }
    public void Pruebita()
    {
        //StopAllCoroutines();
        if (!semaforoActivo)
        {
            StartCoroutine(ActivarSemaforo());
        }
    }

    public IEnumerator ActivarSemaforo() {
        semaforoActivo = true;
        for (int i = 0; i < coloresSemaforo.Count; i++)
        {
            if (i==coloresSemaforo.Count - 1)
            {
                yield return new WaitForSeconds(3.0f);

            }
            else
            {
                yield return new WaitForSeconds(timecolor);
            }
                semaforo.color = coloresSemaforo[i];
            //timecolor++;
        }
        semaforoActivo = false;
    }

    public void SinCoroutine()
    {
        semaforoActivo = true;
       
    }

    public void loadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }
    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        float oldProgres = 0;
        index = 0;
        float changePercent=0.9f/coloresSemaforo.Count;
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            Debug.Log("Operacion en funcion");
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            if (progress > oldProgres + changePercent)
            {
                oldProgres += changePercent;
                semaforo.color = coloresSemaforo[index];
                index++;
            }
            Debug.Log(operation.progress);
            yield return null;
        }
        Debug.Log("Operacion terminada");

    }

}
