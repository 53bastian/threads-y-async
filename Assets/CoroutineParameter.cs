using System.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineParameter : MonoBehaviour
{

    int num;
    void Update(){
        if (Input.GetKeyDown("z"))
        {
            Debug.Log("tecla z");
            StartCoroutine(NumberTimer(1, NumberRandom1));
            StartCoroutine(NumberTimer(3, NumberRandom2));
            StartCoroutine(NumberTimer(5, NumberRandom3));
        }
    }

    IEnumerator NumberTimer(int timer, Action metodo){
        yield return new WaitForSecondsRealtime(timer);
        metodo();
    }
    public void NumberRandom1(){
        num = UnityEngine.Random.Range(1, 9);
        Debug.Log(num);
    }
    public void NumberRandom2(){
        num = UnityEngine.Random.Range(10, 19);
        Debug.Log(num);
    }
    public void NumberRandom3(){
        num = UnityEngine.Random.Range(20, 29);
        Debug.Log(num);
    }

}
