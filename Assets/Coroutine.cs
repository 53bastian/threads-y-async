using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coroutine : MonoBehaviour
{
    int num;
    void Update(){
        if (Input.GetKeyDown("x")){
            Debug.Log("tecla x");
            StartCoroutine(NumberTimer());
        }
    }

    IEnumerator NumberTimer(){
        yield return new WaitForSecondsRealtime(1f);
        NumberRandom1();
        yield return new WaitForSecondsRealtime(3f);
        NumberRandom2();
        yield return new WaitForSecondsRealtime(5f);
        NumberRandom3();
    }

    public void NumberRandom1(){
        num = Random.Range(1, 9);
        Debug.Log(num);
    }
    public void NumberRandom2(){
        num = Random.Range(10, 19);
        Debug.Log(num);
    }
    public void NumberRandom3(){
         num = Random.Range(20, 29);
        Debug.Log(num);
    }
}
